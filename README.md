# Compass DTO Bundle
This bundle is focused to inject request parameters into data transfer objects.

Installation
============

Make sure Composer is installed globally, as explained in the
[installation chapter](https://getcomposer.org/doc/00-intro.md)
of the Composer documentation.

Applications that use Symfony Flex
----------------------------------

Open a command console, enter your project directory and execute:

```console
$ composer require hprotravel/symfony-dto-bundle
```

Applications that don't use Symfony Flex
----------------------------------------

### Step 1: Download the Bundle

Open a command console, enter your project directory and execute the
following command to download the latest stable version of this bundle:

```console
$ composer require hprotravel/symfony-dto-bundle
```

### Step 2: Enable the Bundle

Then, enable the bundle by adding it to the list of registered bundles
in the `config/bundles.php` file of your project:

```php
// config/bundles.php

return [
    // ...
    Compass\DTOBundle\DTOBundle::class => ['all' => true],
];
```

### Step 3: Enable the argument value resolver
```yaml
Compass\DTOBundle\ArgumentResolver\DTOParamValueResolver:
    tags:
        - { name: controller.argument_value_resolver, priority: -2 }
```

How to use
============
Define a controller method parameter with type hint. The type hint must be instance of `\Compass\Compass\Domain\DTO\Request`. If it is symfony will try to resolve the parameters with default configurations. Using the `\Compass\DTOBundle\DTOParamConverter`.

See: `\Compass\DTOBundle\DTOParamConverter::supports`.

Default parameter resolver parameters (see: `\Compass\DTOBundle\OptionsResolver\ParameterOptionsResolver`, `\Compass\DTOBundle\OptionsResolver\DateParameterOptionsResolver`):
```php
#[Parameter(type: 'string', scope: 'request', disabled: false, options: [], undefined: false)]
```
Property attribute example: 
```php
#[Parameter(type: 'string', scope: 'request', path: 'pathOfThisParameter', disabled: false, options: [], undefined: false)]
```

Available property attribute options
===========================
The converter will try to resolve all the things automatically with defaults but you can configure below parameters using the `#[\Compass\DTOBundle\Attribute\Parameter]` attribute and also if you do not define this attribute to property it'll try to resolve the itself too.

type:
----
Variable's type. Available types are: 'string', 'boolean', 'bool', 'integer', 'int' and 'mixed'. Mixed type allows you to apply any type of value into property.

**Warning:** boolean type is exceptional see: `\Symfony\Component\HttpFoundation\ParameterBag::getBoolean`. 

scope:
-----
Variable's scope. Available scopes are: 'request', 'query', 'headers', 'attributes'

path:
----
Variable's path. It's default is property's name but you can customise the path of variable.
Example:
Url: `*.com?testPath=3`
```php
#[Parameter(scope: 'query', path: 'testPath')]
public $differentName;
```

It'll set 3 into $differentName.

**Warning:** This parameter required per property if you do not define, it'll try to resolve the parameter with its property name. It means in above example path will `differentName`.

disabled:
--------
Disable injection for the selected parameter.

options:
--------

    format:
    -------
    Available when you set type to date. You can configure input datetime's format with this property.
    
    timezone:
    --------
    Available when you set type to date. You can configure datetime's timezone with this property.

undefinedable:
--------------
Make property undefinedable. This property makes you able to find undefined properties.

Extra attribute tips
=====================
This attribute can be use at property or class. If you define this property to class it will effect all the classes properties.

    Default parameters overrides --> Class attribute parameters (If exists) overrides --> Property attribute parameters (If exists)

For every parameter it call the method that finds the final injection configs **per property**.
Example:

```php
<?php
namespace Acme\Foo\Request;

use Compass\DTOBundle\Attribute\Parameter;
use Compass\DTOBundle\Request;

#[Parameter(scope: 'query')] 
class DummyRequest implements Request
{
    #[Parameter(type: 'int')]
    public ?int $foo = null;

    #[Parameter(type: 'int')]
    public ?int $bar = null;
}
```
In above class, the `$foo` will inject from `query` scope and the `$bar` will inject from query with
integer type cast.

Null/Undefined variables:
--------
Every defined property that has no default value is nullable and can be undefined.

**Warning:** If you define a default value to property, converter inject the default value instead of null.

Example:

```php
namespace Acme\Foo\Request;

use Compass\DTOBundle\Attribute\Parameter;
use Compass\DTOBundle\Request;
use Compass\DTOBundle\Undefined;

class DummyRequest implements Request
{
    #[Parameter(scope: 'query', undefined: true)]
    public ?string|Undefined $foo = null;
}
```
Results would be:

| ?foo= | ?foo=bar | ?baz=5 |
|---------------|------------------| ---------------- |
| null          | "bar"            | Undefined()      |

Life cycle events
=================

\Compass\DTOBundle\Attribute\PreSet:
---------------------------------------

Defining this attribute onto methods allows you to access properties **before** setting the request parameters into target class.

\Compass\DTOBundle\Attribute\PostSet:
---------------------------------------

Defining this attribute onto methods allows you to access properties **after** setting the request parameters into target class.

The \Compass\DTOBundle\CallableRequest interface
======================================================
You should inject all the simple parameters using above configurations with the `#[\Compass\Compass\Attribute\DTO\Parameter]` attribute but if there is a complex logic that the attribute can not handle you can use this interface as callback method.

In `call()` method you can modify the object's properties using `...$args` variables.
**Tip 1:** Recommended way to hand basic processes is using the life cycle events. Only use this interface if you need to inject anything into target class. 
**Tip 2:** If you dont know what does `...$args` mean see the RFC: https://wiki.php.net/rfc/argument_unpacking.

Example usage:
--------------
### Controller:

```php
<?php
namespace Acme\Foo\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Attribute\Route;
...

class DummyController extends Controller
{
   #[Route('/dummy/{id}', name: 'dummy_route', requirements: ['id' => '\d+'], methods: ['DELETE'])]
   public function __invoke(FooService $fooService, BarService $barService, BazService $bazService, DummyRequest $request): JsonResponse
   {
       $request->call($fooService, $barService);

       return new JsonResponse($bazService->handle($request));
   }
}
```

### Request:

```php
<?php
namespace Acme\Foo\Request;

use Compass\DTOBundle\Attribute\Parameter;
use Compass\DTOBundle\Attribute\PostSet;
use Compass\DTOBundle\Attribute\PreSet;
use Compass\DTOBundle\CallableRequest;

#[Parameter(scope: 'query')]
class DummyRequest implements CallableRequest
{
   #[Parameter(type: 'int')]
   public ?int $foo = null;

   public $bar;

   public $baz;

   #[Parameter(type: 'date', options: ['format' => 'Y-m-d'])]
   public ?\DateTime $fooBar = null;

   public function call(...$args)
   {
       [ $fooService, $barService ] = $args;

        $this->foo = $fooService->aMethod($barService->bMethod($this->foo));
   }

   /**
    * This method will run before setting the parameters 
    */
   #[PreSet]
   public function preSetXMethod(){
       $this->baz = 1;
   }

   /**
    * This method will run after setting the parameters
    */
   #[PostSet]
   public function postSetXMethod(){
       $this->baz = 5;
   }
}
```

Groups
=================

You can group injected properties using `#[\Compass\DTOBundle\Attribute\Group]` attribute.

Example:

```php
<?php
namespace Acme\Foo\Request;

use Compass\DTOBundle\Attribute\Group;
use Compass\DTOBundle\Attribute\Parameter;
use Compass\DTOBundle\CallableRequest;
use Compass\DTOBundle\Undefined;

#[Group(target: 'fooGroup')]
#[Parameter(scope: 'query')]
class DummyRequest implements CallableRequest
{
   #[Parameter(type: 'int', undefined: true)]
   #[Group(target: 'barGroup')]
   public ?int|Undefined $foo = null;

   #[Group(disabled: true)]
   public $bar;

   public $baz;
   
   public array $fooGroup = [];
}
```

Query: `?foo=fooValue&bar=barValue&baz=bazValue&exampleGroup=bug&nextGroup=bug`

`$fooGroup` will be:

```php
[
    'baz' => 'bazValue',
];
```

`$barGroup` will be (defined dynamically):

```php
[
    'foo' => 'fooValue',
];
```

Available Group attribute options
===========================
You can use this attribute on classes (it will apply the group options on every property), or on property

target:
--------
Target group variable. Either the target is defined or not, the converter will put the variables into given target.

disabled:
--------
You can still use property injection but it disables grouping for property or whole class.

Important Notes:
----------------

- Group properties are not injectable
- Group attribute can be used on class or property
- You can modify groups with `#[\Compass\DTOBundle\Attribute\PostSet]` event
- If you disable property injection (`#[\Compass\DTOBundle\Attribute\Parameter(disabled: true)]`) it will also set the property into group. To disable set group's `disabled` option to true.

Contributing
============
If you're having problems, spot a bug, or have a feature suggestion, please log and issue on Github. If you'd like to have a crack yourself, fork the package and make a pull request. Please include tests for any added or changed functionality. If it's a bug, include a regression test.
