## 0.12.0 (December 31, 2024)
  - Feature - CK-849 - Add is_defined function to helpers.php

## 0.11.0 (August 27, 2024)
  - Feature - CK-655 - Replace doctrine annotations with PHP 8 Attributes

## 0.10.0 (March 26, 2024)
  - Feature - CK-555 - Add RequestArrayTypeHandler to handle bulk requests

## 0.9.1 (March 19, 2024)
  - Hotfix - CK-549 - Apply review feedback
  - Hotfix - CK-549 - Fix primitive array casting error and add deprecated warning
  - Hotfix - CK-549 - Implement primitive, enum and enum_array handler
  - Hotfix - CK-549 - Extract to interface
  - Hotfix - CK-549 - Rename castEnum method to cast
  - Hotfix - CK-549 - Extract to EnumTypeHandler class castEnum method
  - Hotfix - CK-549 - Add Php array enum support

## 0.9.0 (March 14, 2024)
  - Feature - CK-549 - Add Php native enum support

## 0.8.0 (January 30, 2024)
  - Feature - CK-495 - Upgrade Symfony version as 6.4 or 7.0 and PHP 8.1

## 0.7.0 (February 03, 2023)
  - Feature - CK-354 - Removed framework extra bundle

## 0.6.0 (October 26, 2022)
  - Feature - CK-264 - Add nested target class transform support and fetch all request params

## 0.5.5 (November 30, 2021)
  - Add "sensio/framework-extra-bundle" support for v6.0

## 0.5.4 (October 26, 2021)
  - Remove Tests folder

## 0.5.3 (October 26, 2021)
  - Rename Tests folder to tests
  - Update README file

## 0.5.2 (October 26, 2021)
  - Remove -compass suffix from package name

## 0.5.1 (October 26, 2021)
  - Change package name with hprotravel-compass/symfony-dto-bundle

## 0.5.0 (October 26, 2021)
  - Move all files to src directory
  - Change namespace with compass

## 0.4.0 (December 22, 2020)
  - README updated
  - Undefined option added with optional choice
  - Undefined variable option removed
  - Group annotation added
  - Undefined variable option added

## 0.3.0 (October 08, 2020)
  - Mixed type support added and 'bool' type alias added to allowed types.
  - Float type support added to Parameter annotation.
  - Option property added into @Parameter annotation.
  - README.md updated
  - DateParameter support added.
  - Minor fixes applied

## 0.2.0 (June 04, 2020)
  - Life cycle events added.
  - Comment fix
  - PSR12 fix commited
  - Travis.yml added
  - Fix README.md

## 0.1.1 (April 28, 2020)
  - Namespace changed
  - README typo fixed
  - Folder name changed
  - Directory structure is changed to symfony's best practice standarts instead of being like library

## 0.1.0 (April 24, 2020)
  - README.md updated
  - Namespace fixes and other requirements updated
  - Tests added
  - Bundle initialised

