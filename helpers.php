<?php

use Compass\DTOBundle\Undefined;

if (!function_exists('is_defined')) {
    /**
     * Check property is undefined or not
     */
    function is_defined(mixed $value): bool
    {
        return !$value instanceof Undefined;
    }
}