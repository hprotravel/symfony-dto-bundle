<?php

namespace Compass\DTOBundle;

use Compass\DTOBundle\TypeHandler\TypeHandlerInterface;

class RequestArrayTypeHandler implements TypeHandlerInterface
{
    private DTOParamConverter $converter;

    public function __construct(DTOParamConverter $converter)
    {
        $this->converter = $converter;
    }

    /**
     * @return array<Request>
     */
    public function cast(mixed $type, mixed $value): array
    {
        $result = [];
        foreach ($value as $item) {
            $result[] = $this->converter->convert(
                $this->getRequestForItem($item),
                $type
            );
        }

        return $result;
    }

    private function getRequestForItem(mixed $item): \Symfony\Component\HttpFoundation\Request
    {
        $request = clone $this->converter->getCurrentRequest();
        $request->request->replace($item);

        return $request;
    }
}