<?php

namespace Compass\DTOBundle;

interface CallableRequest extends Request
{
    public function call(...$args);
}
