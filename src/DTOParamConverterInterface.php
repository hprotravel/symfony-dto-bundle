<?php

namespace Compass\DTOBundle;

use Symfony\Component\HttpFoundation\Request;
use Compass\DTOBundle\Request as RequestInterface;

interface DTOParamConverterInterface
{
    public function convert(Request $request, string $className): RequestInterface;

    public function supports(string $className): bool;
}