<?php

namespace Compass\DTOBundle;

interface DTOParameters
{
    const PROPERTY_SCOPE = 'scope';
    const PROPERTY_PATH = 'path';
    const PROPERTY_TYPE = 'type';
    const PROPERTY_TARGET_CLASS = 'targetClass';
    const PROPERTY_ENUM_TYPE = 'enumType';
    const PROPERTY_REQUEST_TYPE = 'requestType';
    const PROPERTY_NULLABLE = 'nullable';
    const PROPERTY_DISABLED = 'disabled';
    const PROPERTY_OPTIONS = 'options';
    const PROPERTY_UNDEFINED = 'undefined';

    const DEFAULT_TYPE = 'string';
    const DEFAULT_SCOPE = 'request';
    const DEFAULT_TARGET_CLASS = null;
    const DEFAULT_ENUM_CLASS = null;
    const DEFAULT_REQUEST_CLASS = null;
    const DEFAULT_NULLABLE = true;
    const DEFAULT_DISABLED = false;
    const DEFAULT_OPTIONS = [];
    const DEFAULT_UNDEFINED = false;
}
