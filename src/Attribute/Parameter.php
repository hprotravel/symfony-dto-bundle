<?php

namespace Compass\DTOBundle\Attribute;

#[\Attribute(\Attribute::TARGET_PROPERTY | \Attribute::TARGET_CLASS)]
final class Parameter
{
    public function __construct(
        /**
         * NOTE: Trying to convert array to any type is invalid
         * You can convert the data into primitive types
         */
        public ?string $type = null,
        public ?string $scope = null,
        public ?string $path = null,
        public ?string $targetClass = null,
        public ?string $enumType = null,
        public ?string $requestType = null,
        public ?bool $disabled = null,
        public array $options = [],
        public ?bool $undefined = null
    ) {
    }
}
