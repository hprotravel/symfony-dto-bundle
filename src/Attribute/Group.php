<?php

namespace Compass\DTOBundle\Attribute;

#[\Attribute(\Attribute::TARGET_PROPERTY | \Attribute::TARGET_CLASS)]
class Group
{
    public function __construct(
        public ?string $target = null,
        public bool $disabled = false
    ) {
    }
}
