<?php

namespace Compass\DTOBundle\TypeHandler;

class EnumArrayTypeHandler extends EnumTypeHandler
{
    /**
     * @return array<\UnitEnum|\BackedEnum>
     */
    public function cast(string $type, mixed $value): mixed
    {
        return \array_map(
            function ($item) use ($type) {
                return parent::cast($type, $item);
            },
            $value
        );
    }
}
