<?php

namespace Compass\DTOBundle\TypeHandler;

interface TypeHandlerInterface
{
    public function cast(string $type, mixed $value): mixed;
}