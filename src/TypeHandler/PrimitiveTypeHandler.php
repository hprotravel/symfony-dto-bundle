<?php

namespace Compass\DTOBundle\TypeHandler;

class PrimitiveTypeHandler implements TypeHandlerInterface
{

    public function cast(string $type, mixed $value): mixed
    {
        if (\is_array($value) && $type !== 'array') {
            // TODO: add deprecated warning
            return $value;
        }

        \settype($value, $type);

        return $value;
    }
}