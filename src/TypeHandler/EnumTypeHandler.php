<?php

namespace Compass\DTOBundle\TypeHandler;

class EnumTypeHandler implements TypeHandlerInterface
{
    /**
     * @return \UnitEnum|\BackedEnum
     */
    public function cast(string $type, mixed $value): mixed
    {
        $refEnum = new \ReflectionEnum($type);

        if ($refEnum->isSubclassOf(\BackedEnum::class)) {
            if (false === $case = $type::from($value)) {
                throw new \InvalidArgumentException(
                    \sprintf(
                        'Enum class "%s" does not have a case with value "%s"!',
                        $type,
                        $value
                    )
                );
            }

            return $case;
        }

        if ($refEnum->isSubclassOf(\UnitEnum::class)) {
            if (false === $case = $refEnum->getConstant($value)) {
                throw new \InvalidArgumentException(
                    \sprintf(
                        'Enum class "%s" does not have a case with value "%s"!',
                        $type,
                        $value
                    )
                );
            }

            return $case;
        }

        throw new \InvalidArgumentException(
            \sprintf(
                'Enum class must be subclass of "%s" or "%s"!',
                \UnitEnum::class,
                \BackedEnum::class
            )
        );
    }
}