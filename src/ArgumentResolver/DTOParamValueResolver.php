<?php

namespace Compass\DTOBundle\ArgumentResolver;

use Compass\DTOBundle\DTOParamConverter;
use Compass\DTOBundle\DTOParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;

class DTOParamValueResolver implements ValueResolverInterface
{
    private DTOParamConverterInterface $converter;

    public function __construct(PropertyAccessorInterface $propertyAccessor)
    {
        $this->converter = new DTOParamConverter($propertyAccessor);
    }

    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        if (!$this->converter->supports($argument->getType())) {
            return [];
        }

        return [$this->converter->convert($request, $argument->getType())];
    }
}
