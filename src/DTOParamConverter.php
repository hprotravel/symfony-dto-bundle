<?php

namespace Compass\DTOBundle;

use Closure;
use Compass\DTOBundle\Attribute\Group;
use Compass\DTOBundle\Attribute\Parameter;
use Compass\DTOBundle\Attribute\PostSet;
use Compass\DTOBundle\Attribute\PreSet;
use Compass\DTOBundle\Exception\DTOException;
use Compass\DTOBundle\OptionsResolver\DateParameterOptionsResolver;
use Compass\DTOBundle\OptionsResolver\ParameterOptionsResolver;
use Compass\DTOBundle\Request as RequestInterface;
use Compass\DTOBundle\TypeHandler\EnumArrayTypeHandler;
use Compass\DTOBundle\TypeHandler\EnumTypeHandler;
use Compass\DTOBundle\TypeHandler\PrimitiveTypeHandler;
use ReflectionClass;
use ReflectionException;
use ReflectionMethod;
use ReflectionProperty;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Throwable;

final class DTOParamConverter implements DTOParamConverterInterface
{
    protected PropertyAccessorInterface $propertyAccessor;
    protected ParameterOptionsResolver $parameterOptionsResolver;

    /** @var OptionsResolver[] */
    protected array $optionsResolvers = [];

    /** @var Closure[] */
    protected array $castCallbacks = [];

    protected Request $currentRequest;

    public function __construct(PropertyAccessorInterface $propertyAccessor)
    {
        $this->propertyAccessor = $propertyAccessor;
        $this->parameterOptionsResolver = new ParameterOptionsResolver();

        // Define custom option resolvers here to resolve type options
        $this->optionsResolvers['date'] = new DateParameterOptionsResolver();

        // Define custom callbacks to parse values into different types
        $this->castCallbacks['date'] = function ($date, array $parameters) {
            if ($date === null) {
                return null;
            }

            $format = $parameters[DTOParameters::PROPERTY_OPTIONS][DateParameterOptionsResolver::PROPERTY_FORMAT];
            $timezone = $parameters[DTOParameters::PROPERTY_OPTIONS][DateParameterOptionsResolver::PROPERTY_TIMEZONE];

            if ($timezone) {
                return \DateTime::createFromFormat($format, $date, new \DateTimeZone($timezone));
            }

            return \DateTime::createFromFormat($format, $date);
        };

        $this->castCallbacks['mixed'] = function ($value, array $parameters) {
            // Mixed types are not applies any casting
            return $value;
        };

        $booleanCastCallback = function ($value, array $parameters) {
            /**
             * Boolean type is exceptional
             *
             * @See: https://www.w3schools.com/php/filter_validate_boolean.asp
             */
            return filter_var($value, FILTER_VALIDATE_BOOLEAN);
        };

        $this->castCallbacks['bool'] = $booleanCastCallback;
        $this->castCallbacks['boolean'] = $booleanCastCallback;
    }

    public function getCurrentRequest(): Request
    {
        return $this->currentRequest;
    }

    public function convert(Request $request, string $className): RequestInterface
    {
        $this->currentRequest = $request;

        try {
            $this->applyProperties($instance = new $className(), new ReflectionClass($instance), $request);

            return $instance;
        } catch (Throwable $e) {
            throw new DTOException('An error occurred while setting parameters into DTO.', $e);
        }
    }

    protected function applyProperties(RequestInterface $instance, ReflectionClass $reflectionClass, $request): void
    {
        $this->callEvent($reflectionClass, $instance, PreSet::class);

        $groups = $this->findGroups($reflectionClass);

        foreach ($this->getProperties($instance, $reflectionClass) as $parameterName => $parameters) {
            // Do not inject group properties
            if (array_key_exists($parameterName, $groups)) {
                continue;
            }

            $scope = $parameters[DTOParameters::PROPERTY_SCOPE];
            $path = $parameters[DTOParameters::PROPERTY_PATH];

            // If parameter is defined as "undefined" means this parameter is not required or nullable given body
            // Set a new Undefined() instance into request
            if (
                $parameters[DTOParameters::PROPERTY_UNDEFINED] === true
                && $this->isDefined($request, $scope, $path) === false
            ) {
                $this->propertyAccessor->setValue($instance, $parameterName, new Undefined);

                continue;
            }

            $value = $this->getValue(
                $request,
                $scope,
                $path
            );

            $value = $this->castValue($value, $parameters);

            if (
                $value !== null
                || ($value === null && $parameters[DTOParameters::PROPERTY_NULLABLE] === true)
            ) {
                $this->propertyAccessor->setValue($instance, $parameterName, $value);
            }
        }

        $this->groupProperties($groups, $instance);

        $this->callEvent($reflectionClass, $instance, PostSet::class);
    }

    protected function callEvent(ReflectionClass $reflectionClass, RequestInterface $instance, string $eventClass): void
    {
        foreach ($reflectionClass->getMethods(ReflectionMethod::IS_PUBLIC) as $reflectionMethod) {
            $attribute = $this->getReflectionAttributeClass($reflectionMethod, $eventClass);

            if ($attribute === null) {
                continue;
            }

            $reflectionMethod->invoke($instance);
        }
    }

    protected function groupProperties(array $groups, RequestInterface $instance)
    {
        foreach ($groups as $groupName => $relatedProperties) {
            $values = [];

            /** @var ReflectionProperty $property */
            foreach ($relatedProperties as $property) {
                $propertyName = $property->getName();

                if (array_key_exists($propertyName, $groups) === true) {
                    // If property is group property, skip
                    continue;
                }

                $values[$propertyName] = $this->propertyAccessor->getValue($instance, $propertyName);
            }

            $instance->{$groupName} = $values;
        }
    }

    protected function findGroups(ReflectionClass $reflectionClass): array
    {
        $classAttribute = $this->getReflectionAttributeClass($reflectionClass, Group::class);
        $classGroup = null;

        if ($classAttribute !== null && $classAttribute->disabled !== true) {
            $classGroup = $classAttribute->target;
        }

        $groups = [];

        foreach ($reflectionClass->getProperties(ReflectionProperty::IS_PUBLIC) as $property) {
            $group = $classGroup;

            $attribute = $this->getReflectionAttributeClass($property, Group::class);

            if ($attribute !== null && $attribute->disabled === true) {
                $group = null;
            } elseif ($attribute !== null) {
                $group = $attribute->target;
            }

            if (empty($group)) {
                continue;
            }

            $groups[$group][] = $property;
        }

        return $groups;
    }

    /**
     * @param RequestInterface $dto
     * @param ReflectionClass $reflectionClass
     * @return array
     */
    protected function getProperties(RequestInterface $dto, ReflectionClass $reflectionClass): array
    {
        $summary = [];
        $classAttributeParameters = $this->readClassAttributeParameters($reflectionClass);
        foreach ($reflectionClass->getProperties() as $reflectionProperty) {
            $propertyName = $reflectionProperty->getName();
            $parameters = [
                [
                    'path' => $propertyName
                ],
                $classAttributeParameters
            ];

            $propertyAttribute = $this->getReflectionAttributeClass($reflectionProperty, Parameter::class);

            if ($propertyAttribute) {
                if ($propertyAttribute->disabled === true) {
                    // Do not inject parameter
                    continue;
                }

                $parameters[] = $this->readPropertyAttributeParameters($propertyAttribute, $reflectionProperty);
            }

            if ($reflectionProperty->getValue($dto) !== null) {
                // If user define a default value it must not override with null
                $parameters[] = [
                    DTOParameters::PROPERTY_NULLABLE => false
                ];
            }

            // This code (array_replace(...$parameters)) is working like waterfall
            // Every element overrides its previous element
            $summary[$propertyName] = $this->parameterOptionsResolver->resolve(array_replace(...$parameters));

            // If parameter type has resolver, resolve parameters with using it
            if ($propertyAttribute && isset($this->optionsResolvers[$propertyAttribute->type])) {
                $summary[$propertyName][DTOParameters::PROPERTY_OPTIONS] =
                    $this->optionsResolvers[$propertyAttribute->type]->resolve(
                        $summary[$propertyName][DTOParameters::PROPERTY_OPTIONS]
                    );
            }
        }

        return $summary;
    }

    /**
     * @template T
     * @param ReflectionClass|ReflectionMethod|ReflectionProperty $reflection
     * @param class-string<T> $attributeClass
     * @return T
     */
    protected function getReflectionAttributeClass(
        ReflectionClass|ReflectionMethod|ReflectionProperty $reflection,
        string $attributeClass
    ) {
        $attributes = $reflection->getAttributes($attributeClass);

        if ($attributes) {
            return $attributes[0]->newInstance();
        }

        return null;
    }

    protected function readClassAttributeParameters(ReflectionClass $class): array
    {
        $attribute = $this->getReflectionAttributeClass($class, Parameter::class);

        if ($attribute === null) {
            return [];
        }

        // We're filtering the options, because the null
        // values are overriding the parent configurations
        return $this->filterOptions(
            [
                DTOParameters::PROPERTY_TYPE => $attribute->type,
                DTOParameters::PROPERTY_SCOPE => $attribute->scope,
                DTOParameters::PROPERTY_DISABLED => $attribute->disabled,
                DTOParameters::PROPERTY_UNDEFINED => $attribute->undefined,
            ]
        );
    }

    protected function readPropertyAttributeParameters(
        Parameter $attribute,
        \ReflectionProperty $property
    ): array
    {
        $properties = [
            DTOParameters::PROPERTY_SCOPE => $attribute->scope,
            DTOParameters::PROPERTY_PATH => $attribute->path ?? $property->getName(),
            DTOParameters::PROPERTY_TYPE => $attribute->type,
            DTOParameters::PROPERTY_TARGET_CLASS => $attribute->targetClass,
            DTOParameters::PROPERTY_ENUM_TYPE => $attribute->enumType,
            DTOParameters::PROPERTY_REQUEST_TYPE => $attribute->requestType,
            DTOParameters::PROPERTY_DISABLED => $attribute->disabled,
            DTOParameters::PROPERTY_OPTIONS => $attribute->options,
            DTOParameters::PROPERTY_UNDEFINED => $attribute->undefined,
        ];

        // We're filtering the options, because the null
        // values are overriding the parent configurations
        return $this->filterOptions($properties);
    }

    /**
     * @param Request|array $request
     * @param string $scope
     * @param string $path
     * @return bool
     */
    protected function isDefined($request, string $scope, string $path)
    {
        if ($request instanceof Request) {
            return $request->{$scope}->has($path);
        }

        return array_key_exists($path, $request);
    }

    /**
     * @param Request|array $request
     * @param string $scope
     * @param string $path
     * @return mixed
     */
    protected function getValue($request, string $scope, string $path)
    {
        if ($request instanceof Request) {
            if ('*' === $path) {
                return $request->{$scope}->all();
            }

            return $this->propertyAccessor->getValue($request->{$scope}->all(), $this->normalizePath($path, $scope));
        }

        return $this->propertyAccessor->getValue($request, $this->normalizePath($path, $scope));
    }

    /**
     * @param $typeCast
     * @param $targetClass
     * @param $value
     * @return mixed
     * @throws ReflectionException
     */
    protected function castClass($typeCast, $targetClass, $value)
    {
        if ('object' === $typeCast) {
            $instance = new $targetClass();
            $this->applyProperties($instance, new ReflectionClass($instance), (array) $value);

            return $instance;
        }

        if ('array' === $typeCast) {
            if (!is_array($value)) {
                return [];
            }

            $result = [];
            foreach ($value as $request) {
                $instance = new $targetClass();
                $this->applyProperties($instance, new ReflectionClass($instance), (array) $request);
                array_push($result, $instance);
            }

            return $result;
        }

        throw new \InvalidArgumentException(
            sprintf(
                'Type must be "object" or "array" when using "%s" property!',
                DTOParameters::PROPERTY_TARGET_CLASS
            )
        );
    }

    protected function castValue(mixed $value, array $parameters): mixed
    {
        $type = $parameters[DTOParameters::PROPERTY_TYPE];
        $targetClass = $parameters[DTOParameters::PROPERTY_TARGET_CLASS];

        if (null !== $targetClass) {
            return $this->castClass($type, $targetClass, $value);
        }

        // If we apply to typecast into null int returns 0, string returns "", bool returns false
        // It can be crash the application, to prevent this kind of circumstances we're
        // checking the value is null or not.
        if ($value === null) {
            return null;
        }

        // If any callable binded to this type
        if (isset($this->castCallbacks[$type])) {
            return \call_user_func($this->castCallbacks[$type], $value, $parameters);
        }

        $enumType = $parameters[DTOParameters::PROPERTY_ENUM_TYPE];
        $requestType = $parameters[DTOParameters::PROPERTY_REQUEST_TYPE];

        return match ($type) {
            'enum' => (new EnumTypeHandler())->cast($enumType, $value),
            'enum_array' => (new EnumArrayTypeHandler())->cast($enumType, $value),
            'request_array' => (new RequestArrayTypeHandler($this))->cast($requestType, $value),
            default => (new PrimitiveTypeHandler())->cast($type, $value),
        };
    }

    protected function normalizePath(string $path, string $scope): string
    {
        if ($path[0] !== '[') {
            $path = sprintf('[%s]', $path);
        }

        // Because symfony's header bag stores variables in multidimensional arrays
        if ($scope === 'headers') {
            $path = strtolower($path) . '[0]';
        }

        return $path;
    }

    protected function filterOptions(array $options): array
    {
        return array_filter(
            $options,
            static function ($value) {
                return $value !== null;
            }
        );
    }

    public function supports(string $className): bool
    {
        return is_subclass_of($className, RequestInterface::class);
    }
}
