<?php

namespace Tests\Fixtures\Enum;

class ClassType
{
    const FOO = 'foo';
    const BAR = 'bar';
}