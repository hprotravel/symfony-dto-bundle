<?php

namespace Tests\Fixtures\Enum;

enum BakedEnumType: string
{
    case FOO = 'foo';
    case BAR = 'bar';
}