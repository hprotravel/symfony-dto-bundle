<?php

namespace Tests\Fixtures\Enum;

enum EnumType
{
    case FOO;
    case BAR;
}