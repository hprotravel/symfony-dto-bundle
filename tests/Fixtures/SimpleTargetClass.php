<?php

namespace Tests\Fixtures;

use Compass\DTOBundle\Attribute\Parameter;
use Compass\DTOBundle\Request;

class SimpleTargetClass implements Request
{
    #[Parameter(type: 'object', scope: 'request', path: '*', targetClass: SimpleClass::class)]
    public ?SimpleClass $simpleClass = null;

    /**
     * @var SimpleClass[]
     */
    #[Parameter(type: 'array', scope: 'request', path: '*', targetClass: SimpleClass::class)]
    public array $simpleClasses = [];
}
