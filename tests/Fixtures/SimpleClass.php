<?php

namespace Tests\Fixtures;

use Compass\DTOBundle\Request;

class SimpleClass extends Serializable implements Request
{
    public $testProperty;

    public function __construct($testProperty = null)
    {
        $this->testProperty = $testProperty;
    }
}
