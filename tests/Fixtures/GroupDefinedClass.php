<?php

namespace Tests\Fixtures;

use Compass\DTOBundle\Attribute\Group;
use Compass\DTOBundle\Attribute\Parameter;
use Compass\DTOBundle\Request;

#[Group(target: 'groupTarget')]
class GroupDefinedClass implements Request
{
    /** @var string */
    public $simpleProperty;

    /**
     * @var string
     */
    #[Parameter(scope: 'query', undefined: true)]
    public $undefinedableProperty;

    /**
     * @var string
     */
    #[Parameter(scope: 'query', undefined: true)]
    public $nullableProperty;

    /**
     * @var string
     */
    #[Group(target: 'nextTarget')]
    public $annotationDefinedProperty;

    /**
     * @var string
     */
    #[Group(disabled: true)]
    public $disabledGroupProperty;

    /**
     * @var string
     */
    #[Parameter(disabled: true)]
    public $parameterDisabledProperty;

    /** @var array */
    public $groupTarget;

    /** @var array */
    public $nextTarget;
}
