<?php

namespace Tests\Fixtures;

use Compass\DTOBundle\Attribute\Parameter;
use Compass\DTOBundle\Request;

/**
 * Class ClassPropertyDefinedClass
 * @package Compass\DTOBundle\Fixtures
 */
#[Parameter(scope: 'query')]
class ClassPropertyDefinedClass implements Request
{
    /**
     * @var string|null
     */
    #[Parameter(scope: 'attributes')]
    public $testScopeProperty;

    /**
     * We did not define scope attribute here, class parameter annotation must override it
     *
     * @var int
     */
    #[Parameter(type: 'int')]
    public $testAnotherDefinitionProperty;
}
