<?php

namespace Tests\Fixtures;

use Compass\DTOBundle\Attribute\Parameter;
use Compass\DTOBundle\Request;

class SimpleRequest implements Request
{
    #[Parameter(type: 'array', scope: 'query', path: '*')]
    public array $requestQuery = [];

    #[Parameter(type: 'array', scope: 'request', path: '*')]
    public array $requestBody = [];
}
