<?php

namespace Tests\Fixtures;

use Compass\DTOBundle\Attribute\Parameter;
use Compass\DTOBundle\Request;

class SimpleBulkRequest implements Request
{
    #[Parameter(type: 'request_array', path: '*', requestType: SimpleRequestWithEnum::class)]
    public array $requests = [];
}