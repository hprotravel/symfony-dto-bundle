<?php

namespace Tests\Fixtures;

use Compass\DTOBundle\Attribute\PreSet;
use Compass\DTOBundle\Request;

class PreSetEventDefinedClass implements Request
{
    /**
     * @var string
     */
    public $testProperty;

    #[PreSet]
    public function callBeforeParameters()
    {
        $this->testProperty = 'testValue';
    }
}
