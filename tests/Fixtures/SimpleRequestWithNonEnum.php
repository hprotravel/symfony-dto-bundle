<?php

namespace Tests\Fixtures;

use Compass\DTOBundle\Attribute\Parameter;
use Compass\DTOBundle\Request;
use Tests\Fixtures\Enum\ClassType;

class SimpleRequestWithNonEnum implements Request
{
    public ?string $foo = null;

    public ?int $bar = null;

    #[Parameter(type: 'enum', enumType: ClassType::class)]
    public ?ClassType $classType = null;
}
