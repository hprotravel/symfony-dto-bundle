<?php

namespace Tests\Fixtures;

use Compass\DTOBundle\Attribute\Parameter;
use Compass\DTOBundle\Request;
use Compass\DTOBundle\Undefined;

class PropertyDefinedClass implements Request
{
    /**
     * @var string|null
     */
    #[Parameter]
    public $testProperty;

    /**
     * @var string|null
     */
    #[Parameter(scope: 'attributes')]
    public $testScopeProperty;

    /**
     * @var string|null
     */
    #[Parameter]
    public $testNotNullableProperty = 'defaultValue';

    /**
     * @var string|null
     */
    public $testDefaultValueProperty = 'defaultValueWithoutAnnotation';

    /**
     * @var string|null
     */
    #[Parameter(path: 'examplePath')]
    public $testPathProperty;


    /**
     * @var int|null
     */
    #[Parameter(type: 'int')]
    public $testTypeProperty;

    /**
     * @var mixed
     */
    #[Parameter(type: 'mixed')]
    public $testMixedPath;

    /**
     * @var boolean
     */
    #[Parameter(type: 'boolean')]
    public $testBooleanProperty;

    /**
     * @var boolean
     */
    #[Parameter(type: 'bool')]
    public $testBoolProperty;

    /**
     * @var boolean
     */
    #[Parameter(type: 'boolean')]
    public $testBooleanWithDefaultProperty = true;

    /**
     * @var boolean
     */
    #[Parameter(type: 'float')]
    public $testFloatProperty;

    /**
     * @var boolean
     */
    #[Parameter(disabled: true)]
    public $testDisabledProperty;

    /**
     * @var string
     */
    #[Parameter(scope: 'headers')]
    public $testHeaderProperty;

    /**
     * @var \DateTime
     */
    #[Parameter(type: 'date', undefined: true)]
    public $testDateProperty;
    
    /**
     * @var \DateTime
     */
    #[Parameter(type: 'date', options: ['format' => 'm.d.Y'])]
    public $testDateWithFormatProperty;
    
    /**
     * @var \DateTime
     */
    #[Parameter(type: 'date', options: ['timezone' => 'Europe/London'])]
    public $testDateWithTimeZoneProperty;

    /**
     * @var string|Undefined
     */
    #[Parameter(undefined: true)]
    public $testUndefinedableProperty;
}
