<?php

namespace Tests\Fixtures;

use Compass\DTOBundle\CallableRequest;

class CallableMethodDefinedClass implements CallableRequest
{
    /**
     * @var string
     */
    public $testProperty;

    public function call(...$args)
    {
        $this->testProperty = $args[0];
    }
}
