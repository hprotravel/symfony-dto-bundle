<?php

namespace Tests\Fixtures;

use Compass\DTOBundle\Attribute\PostSet;
use Compass\DTOBundle\Request;

class PostSetEventDefinedClass implements Request
{
    /**
     * @var string
     */
    public $testProperty;

    #[PostSet]
    public function callAfterParameters()
    {
        $this->testProperty = [
            $this->testProperty
        ];
    }
}
