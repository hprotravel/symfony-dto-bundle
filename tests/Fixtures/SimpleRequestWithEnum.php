<?php

namespace Tests\Fixtures;

use Compass\DTOBundle\Attribute\Parameter;
use Compass\DTOBundle\Request;
use Tests\Fixtures\Enum\BakedEnumType;
use Tests\Fixtures\Enum\EnumType;

class SimpleRequestWithEnum implements Request
{
    #[Parameter(type: 'string', scope: 'query')]
    public ?string $foo = null;
    #[Parameter(type: 'integer', scope: 'attributes')]
    public ?int $bar = null;
    #[Parameter(type: 'string', scope: 'headers')]
    public ?string $baz = null;

    #[Parameter(type: 'enum', enumType: EnumType::class)]
    public ?EnumType $enumType = null;
    #[Parameter(type: 'enum', enumType: BakedEnumType::class)]
    public ?BakedEnumType $bakedEnumType = null;

    #[Parameter(type: 'enum_array', enumType: EnumType::class)]
    public ?array $enumTypes = null;
    #[Parameter(type: 'enum_array', enumType: BakedEnumType::class)]
    public ?array $bakedEnumTypes = null;
}
