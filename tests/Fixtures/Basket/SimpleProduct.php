<?php

namespace Tests\Fixtures\Basket;

use Compass\DTOBundle\Attribute\Parameter;
use Compass\DTOBundle\Request;
use Tests\Fixtures\Serializable;

class SimpleProduct extends Serializable implements Request
{
    public $id;

    public $name;

    #[Parameter(type: 'object', targetClass: SimplePrice::class)]
    public ?SimplePrice $price = null;

    public function __construct($id = null, $name = null, SimplePrice $price = null)
    {
        $this->id = $id;
        $this->name = $name;
        $this->price = $price;
    }
}