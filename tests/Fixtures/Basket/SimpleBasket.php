<?php

namespace Tests\Fixtures\Basket;

use Compass\DTOBundle\Attribute\Parameter;
use Compass\DTOBundle\Request;
use Tests\Fixtures\Serializable;

class SimpleBasket extends Serializable implements Request
{
    public $id;

    /**
     * @var SimpleProduct[]
     */
    #[Parameter(type: 'array', targetClass: SimpleProduct::class)]
    public array $products = [];

    public function __construct($id = null, array $products = [])
    {
        $this->id = $id;
        $this->products = $products;
    }
}
