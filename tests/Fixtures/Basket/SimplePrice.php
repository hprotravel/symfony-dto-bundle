<?php

namespace Tests\Fixtures\Basket;

use Compass\DTOBundle\Request;
use Tests\Fixtures\Serializable;

class SimplePrice extends Serializable implements Request
{
    public $currency;

    public $amount;

    public function __construct($currency = null, $amount = null)
    {
        $this->currency = $currency;
        $this->amount = $amount;
    }
}