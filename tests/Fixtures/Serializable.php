<?php

namespace Tests\Fixtures;

abstract class Serializable implements \JsonSerializable
{
    public function jsonSerialize(): mixed
    {
        return get_object_vars($this);
    }
}
