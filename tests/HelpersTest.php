<?php

namespace Tests;

use Compass\DTOBundle\Undefined;
use PHPUnit\Framework\TestCase;
use Tests\Fixtures\PropertyDefinedClass;

class HelpersTest extends TestCase
{
    private PropertyDefinedClass $dto;

    protected function setUp(): void
    {
        $this->dto = new PropertyDefinedClass();
    }

    public function testIsDefinedTrue(): void
    {
        $this->assertTrue(\is_defined($this->dto->testUndefinedableProperty));
    }

    public function testDefinedFalse(): void
    {
        $this->dto->testUndefinedableProperty = new Undefined();

        $this->assertFalse(\is_defined($this->dto->testUndefinedableProperty));
    }
}