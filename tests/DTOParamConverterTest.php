<?php

namespace Tests;

use Compass\DTOBundle\Exception\DTOException;
use Compass\DTOBundle\DTOParamConverter;
use Tests\Fixtures\CallableMethodDefinedClass;
use Tests\Fixtures\ClassPropertyDefinedClass;
use Tests\Fixtures\Enum\BakedEnumType;
use Tests\Fixtures\Enum\EnumType;
use Tests\Fixtures\GroupDefinedClass;
use Tests\Fixtures\NotSupportedClass;
use Tests\Fixtures\PostSetEventDefinedClass;
use Tests\Fixtures\PreSetEventDefinedClass;
use Tests\Fixtures\PropertyDefinedClass;
use Tests\Fixtures\SimpleBulkRequest;
use Tests\Fixtures\SimpleClass;
use Compass\DTOBundle\Undefined;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Tests\Fixtures\SimpleRequestWithEnum;
use Tests\Fixtures\SimpleRequestWithNonEnum;
use Tests\Fixtures\SimpleTargetClass;
use Tests\Fixtures\Basket\SimpleBasket;
use Tests\Fixtures\Basket\SimplePrice;
use Tests\Fixtures\Basket\SimpleProduct;
use Tests\Fixtures\SimpleRequest;

class DTOParamConverterTest extends TestCase
{
    protected DTOParamConverter $converter;

    public function setUp(): void
    {
        parent::setUp();

        $this->converter = new DTOParamConverter(new PropertyAccessor());
    }

    public function testSupports()
    {
        $this->assertTrue($this->converter->supports(SimpleClass::class));
        $this->assertFalse($this->converter->supports(NotSupportedClass::class));
    }

    /**
     * Simple convert process
     */
    public function testSimpleParameter()
    {
        $request = new Request([], ['testProperty' => 'test']);

        /** @var SimpleClass $target */
        $target = $this->converter->convert($request, SimpleClass::class);
        $this->assertInstanceOf(SimpleClass::class, $target);
        $this->assertSame($target->testProperty, 'test');
    }

    public function testSimpleRequest()
    {
        $queryString = ['limit' => 100, 'page' => 5];
        $requestBody = [
            ['name' => 'John', 'surname' => 'Stone'],
            ['name' => 'Trevor', 'surname' => 'Virtue']
        ];

        $request = new Request($queryString, $requestBody);

        /** @var SimpleRequest $target */
        $target = $this->converter->convert($request, SimpleRequest::class);
        $this->assertInstanceOf(SimpleRequest::class, $target);
        $this->assertSame($queryString, $target->requestQuery);
        $this->assertSame($requestBody, $target->requestBody);
    }

    public function testSimpleTargetClass()
    {
        $simpleClass = new SimpleClass('Simple Class 0');

        $request = new Request([], json_decode(json_encode($simpleClass), true));

        /** @var SimpleTargetClass $target */
        $target = $this->converter->convert($request, SimpleTargetClass::class);
        $this->assertInstanceOf(SimpleTargetClass::class, $target);
        $this->assertEquals($simpleClass, $target->simpleClass);
    }

    public function testSimpleTargetClassNull()
    {
        $request = new Request([], []);

        /** @var SimpleTargetClass $target */
        $target = $this->converter->convert($request, SimpleTargetClass::class);
        $this->assertInstanceOf(SimpleTargetClass::class, $target);
        $this->assertInstanceOf(SimpleClass::class, $target->simpleClass);
        $this->assertEquals(new SimpleClass(), $target->simpleClass);
    }

    public function testSimpleTargetClassArray()
    {
        $simpleClasses = [
            new SimpleClass('Simple Class 1'),
            new SimpleClass('Simple Class 2')
        ];

        $request = new Request([], json_decode(json_encode($simpleClasses), true));

        /** @var SimpleTargetClass $target */
        $target = $this->converter->convert($request, SimpleTargetClass::class);
        $this->assertInstanceOf(SimpleTargetClass::class, $target);
        $this->assertEquals($simpleClasses, $target->simpleClasses);
    }

    public function testSimpleTargetClassArrayEmpty()
    {
        $request = new Request([], []);

        /** @var SimpleTargetClass $target */
        $target = $this->converter->convert($request, SimpleTargetClass::class);
        $this->assertInstanceOf(SimpleTargetClass::class, $target);
        $this->assertEquals([], $target->simpleClasses);
    }

    public function testSimpleTargetClassArrayNull()
    {
        $request = new Request([], [[], []]);

        /** @var SimpleTargetClass $target */
        $target = $this->converter->convert($request, SimpleTargetClass::class);
        $this->assertInstanceOf(SimpleTargetClass::class, $target);
        $this->assertEquals([new SimpleClass(), new SimpleClass()], $target->simpleClasses);
    }

    public function testNestedTargetClass()
    {
        $basket = new SimpleBasket(
            15,
            [
                new SimpleProduct(8850, 'Product 1', new SimplePrice('USD', 59.99)),
                new SimpleProduct(8853, 'Product 2', new SimplePrice('TRY', 39.99))
            ]
        );

        $request = new Request([], json_decode(json_encode($basket), true));

        /** @var SimpleBasket $target */
        $target = $this->converter->convert($request, SimpleBasket::class);
        $this->assertInstanceOf(SimpleBasket::class, $target);
        $this->assertEquals($basket->id, $target->id);
        $this->assertEquals($basket->products, $target->products);
    }

    public function testCallableMethod()
    {
        $request = new Request();

        /** @var CallableMethodDefinedClass $target */
        $target = $this->converter->convert($request, CallableMethodDefinedClass::class);
        // Call the callback method
        $target->call('givenValue', 'givenValue2', 'anotherValue');

        $this->assertInstanceOf(CallableMethodDefinedClass::class, $target);
        $this->assertSame($target->testProperty, 'givenValue');
    }

    public function testPreSetEventDefinedClass()
    {
        $request = new Request();

        /** @var PreSetEventDefinedClass $target */
        $target = $this->converter->convert($request, PreSetEventDefinedClass::class);

        $this->assertInstanceOf(PreSetEventDefinedClass::class, $target);
        // We set this value in fixture class
        $this->assertSame($target->testProperty, 'testValue');
    }

    public function testPostSetEventDefinedClass()
    {
        $request = new Request([], ['testProperty' => 'test']);

        /** @var PostSetEventDefinedClass $target */
        $target = $this->converter->convert($request, PostSetEventDefinedClass::class);

        $this->assertInstanceOf(PostSetEventDefinedClass::class, $target);
        // We sent it text, but we transformed this value into an array
        $this->assertSame($target->testProperty, ['test']);
    }

    /**
     * Simple convert process with missing value
     */
    public function testSimpleParameterWithMissingValue()
    {
        $request = new Request();

        /** @var SimpleClass $target */
        $target = $this->converter->convert($request, SimpleClass::class);
        $this->assertInstanceOf(SimpleClass::class, $target);

        /**
         * Because in default properties are nullable
         * @see \Compass\DTOBundle\DTOParamConverter::PROPERTY_NULLABLE
         **/
        $this->assertNull($target->testProperty);
    }

    /**
     * We're just testing the property override process
     * Other things are tested above
     */
    public function testClassScopeDefinition()
    {
        $request = new Request(
            ['testScopeProperty' => 'wrongValue', 'testAnotherDefinitionProperty' => 1 ],
            ['testScopeProperty' => 'wrongValue2'],
            ['testScopeProperty' => 'trueValue']
        );

        /** @var ClassPropertyDefinedClass $target */
        $target = $this->converter->convert($request, ClassPropertyDefinedClass::class);
        $this->assertInstanceOf(ClassPropertyDefinedClass::class, $target);
        $this->assertSame($target->testScopeProperty, 'trueValue');
        $this->assertSame($target->testAnotherDefinitionProperty, 1);
    }

    private function assertDefinition(Request $request, string $propertyName, $actualValue)
    {
        /** @var PropertyDefinedClass $target */
        $target = $this->converter->convert($request, PropertyDefinedClass::class);
        $this->assertInstanceOf(PropertyDefinedClass::class, $target);

        if (is_object($actualValue)) {
            $this->assertInstanceOf(get_class($target->$propertyName), new Undefined);

            return;
        }

        $this->assertSame($target->$propertyName, $actualValue);
    }

    /**
     * Same with below pointed test
     *
     * @see DTOParamConverterTest::testSimpleParameter
     */
    public function testEmptyDefinition()
    {
        $request = new Request([], ['testProperty' => 'test']);
        $this->assertDefinition($request, 'testProperty', 'test');
    }

    /**
     * We're testing scope parameter
     */
    public function testScopeDefinition()
    {
        $request = new Request([], ['testScopeProperty' => 'wrongValue'], ['testScopeProperty' => 'trueValue']);
        $this->assertDefinition($request, 'testScopeProperty', 'trueValue');
    }

    /**
     * We're testing default value definition
     * It must not override default parameter with null
     */
    public function testNotNullableDefinition()
    {
        $request = new Request();
        $this->assertDefinition($request, 'testNotNullableProperty', 'defaultValue');
    }

    /**
     * We're testing default value definition
     * without any parameter here
     */
    public function testDefaultValueDefinition()
    {
        $request = new Request();
        $this->assertDefinition($request, 'testDefaultValueProperty', 'defaultValueWithoutAnnotation');
    }

    /**
     * We're testing path parameter
     */
    public function testPathDefinition()
    {
        $request = new Request([], ['examplePath' => 'testValue']);
        $this->assertDefinition($request, 'testPathProperty', 'testValue');
    }

    /**
     * We're testing type parameter
     */
    public function testTypeDefinition()
    {
        $request = new Request([], ['testTypeProperty' => '1']);
        $this->assertDefinition($request, 'testTypeProperty', 1);
    }

    public function testMixedTypeDefinition()
    {
        $request = new Request([], ['testMixedPath' => 1 ]);
        $this->assertDefinition($request, 'testMixedPath', 1);
    }

    public function floatDataProvider(): array
    {
        return [
            [
                5.30,
                5.30
            ],
            [
                '5.30',
                5.30
            ]
        ];
    }

    /**
     * @dataProvider floatDataProvider
     *
     * @param array $data
     * @param bool $sameWith
     */
    public function testFloatTypeDefinition($data, $sameWith)
    {
        $request = new Request([], ['testFloatProperty' => $data ]);
        $this->assertDefinition($request, 'testFloatProperty', $sameWith);
    }

    public function booleanDataProvider(): array
    {
        return [
            [
                true, //Equals to below definition
                true
            ],
            [
                'true',
                true
            ],
            [
                '1',
                true
            ],
            [
                false,
                false
            ],
            [
                'false',
                false
            ],
            [
                '0',
                false
            ],
            [
                null,
                null
            ]
        ];
    }

    /**
     * @dataProvider booleanDataProvider
     *
     * @param array $data
     * @param bool $sameWith
     */
    public function testBooleanTypeDefinition($data, $sameWith)
    {
        $request = new Request([], ['testBooleanProperty' => $data ]);
        $this->assertDefinition($request, 'testBooleanProperty', $sameWith);
    }

    /**
     * @dataProvider booleanDataProvider
     *
     * @param array $data
     * @param bool $sameWith
     */
    public function testBoolTypeDefinition($data, $sameWith)
    {
        $request = new Request([], ['testBoolProperty' => $data ]);
        $this->assertDefinition($request, 'testBoolProperty', $sameWith);
    }

    public function booleanWithDefaultValueDataProvider(): array
    {
        return [
            [
                true, //Equals to below definition
                true
            ],
            [
                'true',
                true
            ],
            [
                '1',
                true
            ],
            [
                false,
                false
            ],
            [
                'false',
                false
            ],
            [
                '0',
                false
            ],
            [
                null,
                true
            ]
        ];
    }

    /**
     * @dataProvider booleanWithDefaultValueDataProvider
     *
     * @param array $data
     * @param bool $sameWith
     */
    public function testBooleanTypeWithDefaultDefinition($data, bool $sameWith)
    {
        $request = new Request([], ['testBooleanWithDefaultProperty' => $data ]);
        $this->assertDefinition($request, 'testBooleanWithDefaultProperty', $sameWith);
    }

    public function testDisabledDefinition()
    {
        $request = new Request([], ['testDisabledProperty' => 5 ]);
        $this->assertDefinition($request, 'testDisabledProperty', null);
    }

    public function testHeaderScopeDefinition()
    {
        $request = new Request();
        $request->headers->set('testHeaderProperty', '5');
        $this->assertDefinition($request, 'testHeaderProperty', '5');
    }

    public function testUndefinedableUndefinedValueDefinition()
    {
        $request = new Request();
        $this->assertDefinition($request, 'testUndefinedableProperty', new Undefined);
    }

    public function testUndefinedableDefinedValueDefinition()
    {
        $request = new Request([], ['testUndefinedableProperty' => '1']);
        $this->assertDefinition($request, 'testUndefinedableProperty', '1');
    }

    private function assertDateDefinition(Request $request, string $propertyName, $actualValue)
    {
        /** @var PropertyDefinedClass $target */
        $target = $this->converter->convert($request, PropertyDefinedClass::class);
        $this->assertInstanceOf(PropertyDefinedClass::class, $target);

        /** @var \DateTime|null $computedDate */
        $computedDate = $target->$propertyName;

        if ($actualValue === null) {
            $this->assertNull($target->$propertyName);
        } elseif ($actualValue instanceof Undefined) {
            $this->assertInstanceOf(get_class($target->$propertyName), new Undefined);
        } else {
            $this->assertInstanceOf(\DateTime::class, $computedDate);
            $this->assertSame($computedDate->getTimestamp(), $actualValue->getTimestamp());
            $this->assertSame($computedDate->getTimezone()->getName(), $actualValue->getTimezone()->getName());
        }
    }

    public function testUndefinedDateDefinition()
    {
        $request = new Request();
        $this->assertDateDefinition(
            $request,
            'testDateProperty',
            new Undefined()
        );
    }

    public function testNullDefinedDateDefinition()
    {
        $request = new Request([], ['testDateProperty' => null]);
        $this->assertDateDefinition(
            $request,
            'testDateProperty',
            null
        );
    }

    public function testSimpleDateDefinition()
    {
        $date = new \DateTime();
        $request = new Request([], ['testDateProperty' => $date->format('Y-m-d H:i:s')]);
        $this->assertDateDefinition(
            $request,
            'testDateProperty',
            $date
        );
    }

    public function testFormatDateDefinition()
    {
        $date = new \DateTime();
        $request = new Request([], ['testDateWithFormatProperty' => $date->format('m.d.Y')]);
        $this->assertDateDefinition(
            $request,
            'testDateWithFormatProperty',
            $date
        );
    }

    public function testTimeZoneDateDefinition()
    {
        $date = new \DateTime('now', new \DateTimeZone('Europe/London'));
        $request = new Request([], ['testDateWithTimeZoneProperty' => $date->format('Y-m-d H:i:s')]);
        $this->assertDateDefinition(
            $request,
            'testDateWithTimeZoneProperty',
            $date
        );
    }

    private function assertGroupDefinition(Request $request, array $expectedValues)
    {
        /** @var GroupDefinedClass $target */
        $target = $this->converter->convert($request, GroupDefinedClass::class);
        $this->assertInstanceOf(GroupDefinedClass::class, $target);

        foreach ($expectedValues as $index => $value) {
            $this->assertObjectHasProperty($index, $target);
            $this->assertTrue(is_array($target->{$index}));

            $this->assertEmpty(
                array_diff_assoc($target->{$index}, $value),
                sprintf(
                    'Group value is not valid. Given: "%s", expected: "%s"',
                    implode(',', $target->{$index}),
                    implode(',', $value)
                )
            );
        }
    }

    public function groupDataProvider(): array
    {
        return [
            [ // Case 1
                [ // $data
                    [ // $request->query
                        'undefinedableProperty' => 'undefinedableProperty',
                        'nullableProperty' => null,
                    ],
                    [ // $request->request
                        'simpleProperty' => 'test',
                        'annotationDefinedProperty' => 'testAnnotation',
                        'disabledGroupProperty' => 'testDisabled',
                        // Injecting group property is not a valid operation
                        'groupTarget' => 'notValidParameter',
                        // Injecting disabled property into group is not a valid operation
                        'parameterDisabledProperty' => 'testDisabledParameter'
                    ]
                ],
                [ // $sameWith
                    'groupTarget' => [
                        'simpleProperty' => 'test',
                        'undefinedableProperty' => 'undefinedableProperty',
                        'nullableProperty' => null,
                        'parameterDisabledProperty' => null
                    ],
                    'nextTarget' => [
                        'annotationDefinedProperty' => 'testAnnotation'
                    ]
                ]
            ],
        ];
    }

    /**
     * @dataProvider groupDataProvider
     */
    public function testGroupDefinition(array $data, array $sameWith)
    {
        $request = new Request(...$data);

        $this->assertGroupDefinition($request, $sameWith);
    }

    public function testSimpleRequestWithEnumThrowsException()
    {
        $this->expectException(DTOException::class);

        $request = new Request([], ['classType' => 'bar']);

        $this->converter->convert($request, SimpleRequestWithNonEnum::class);
    }

    public function testSimpleRequestWithBakedEnumInvalidValue()
    {
        $this->expectException(DTOException::class);

        $request = new Request([], ['enumType' => 'foo', 'bakedEnumType' => 'baz']);

        $this->converter->convert($request, SimpleRequestWithEnum::class);
    }

    public function testSimpleRequestWithEnumValue()
    {
        $request = new Request([],
            [
                'enumType' => 'FOO',
                'bakedEnumType' => 'bar',
                'enumTypes' => ['FOO', 'BAR'],
                'bakedEnumTypes' => ['foo', 'bar']
            ]);

        /** @var SimpleRequestWithEnum $target */
        $target = $this->converter->convert($request, SimpleRequestWithEnum::class);

        $this->assertInstanceOf(SimpleRequestWithEnum::class, $target);

        $this->assertSame($target->enumType, EnumType::FOO);
        $this->assertSame($target->bakedEnumType, BakedEnumType::BAR);
        $this->assertSame($target->enumTypes, [EnumType::FOO, EnumType::BAR]);
        $this->assertSame($target->bakedEnumTypes, [BakedEnumType::FOO, BakedEnumType::BAR]);
    }

    public function testSimpleBulkRequest()
    {
        $request = new Request(
            ['foo' => 'FOO'], // query
            [
                [
                    'enumType' => 'FOO',
                    'bakedEnumType' => 'bar',
                    'enumTypes' => ['FOO', 'BAR'],
                    'bakedEnumTypes' => ['foo', 'bar'],
                ],
                [
                    'enumType' => 'BAR',
                    'bakedEnumType' => 'foo',
                    'enumTypes' => ['FOO', 'BAR'],
                    'bakedEnumTypes' => ['foo', 'bar'],
                ],
            ], // request
            [
                'bar' => 99,
            ], // attributes
            [],
            [],
            [
                'baz' => 'BAZ',
            ], // headers
        );

        $target = $this->converter->convert($request, SimpleBulkRequest::class);

        $this->assertInstanceOf(SimpleBulkRequest::class, $target);
        $this->assertContainsOnlyInstancesOf(SimpleRequestWithEnum::class, $target->requests);

        $this->assertSame($target->requests[0]->foo, 'FOO');
        $this->assertSame($target->requests[0]->bar, 99);
        $this->assertSame($target->requests[0]->enumType, EnumType::FOO);
        $this->assertSame($target->requests[0]->bakedEnumType, BakedEnumType::BAR);
        $this->assertSame($target->requests[0]->enumTypes, [EnumType::FOO, EnumType::BAR]);
        $this->assertSame($target->requests[0]->bakedEnumTypes, [BakedEnumType::FOO, BakedEnumType::BAR]);

        $this->assertSame($target->requests[1]->foo, 'FOO');
        $this->assertSame($target->requests[1]->bar, 99);
        $this->assertSame($target->requests[1]->enumType, EnumType::BAR);
        $this->assertSame($target->requests[1]->bakedEnumType, BakedEnumType::FOO);
        $this->assertSame($target->requests[1]->enumTypes, [EnumType::FOO, EnumType::BAR]);
        $this->assertSame($target->requests[1]->bakedEnumTypes, [BakedEnumType::FOO, BakedEnumType::BAR]);
    }
}
